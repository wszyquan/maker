package ldh.maker.component;

import javafx.scene.control.TreeItem;
import ldh.maker.vo.TreeNode;

/**
 * Created by ldh on 2017/4/6.
 */
public class FlutterColumnUi extends ColumnUi {

    public FlutterColumnUi(TreeItem<TreeNode> treeItem, String dbName, String tableName, CodeUi codeUi) {
        super(treeItem, dbName, tableName, codeUi);
    }
}
