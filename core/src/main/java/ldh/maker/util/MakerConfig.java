package ldh.maker.util;

import java.util.Map;
import java.util.TreeMap;

public class MakerConfig {
	
	private static MakerConfig instance = new MakerConfig();

	public static MakerConfig getInstance() {
		return instance;
	}

	private MakerConfig(){
	}

	private volatile String[] mapperNames = {"insert","insertSelective", "update", "updateNotnull", "delete", "getById", "getByUnique", "getWithById", "getWithAssociatesById", "getJoinById", "getJoinAssociatesById", "getManyJoinById", "getManyJoinAllById", "getManyToMany", "find", "findTotal", "findByJoin", "findTotalByJoin"};

	private volatile String[] controllerMethodNames = {"save", "saveJson", "toEdit", "view", "viewJson", "list", "listJson", "deleteJson"};

	private volatile Map<String, Map<String, Boolean>> business = new TreeMap<String, Map<String, Boolean>>();

	private volatile Map<String, Map<String, Boolean>> controllerNameMappers = new TreeMap<String, Map<String, Boolean>>();

	public Map<String, Boolean> getFunctionMap(String tableName) {
		return business.get(tableName);
	}

	public void addFunctionMap(String tableName, Map<String, Boolean> map) {
		business.put(tableName, map);
	}

	public Map<String, Boolean> getControllerMap(String tableName) {
		return controllerNameMappers.get(tableName);
	}

	public void addControllerMap(String tableName, Map<String, Boolean> map) {
		controllerNameMappers.put(tableName, map);
	}
}
