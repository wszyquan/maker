package ldh.maker.component;

import javafx.geometry.Side;
import javafx.scene.Node;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TreeItem;
import ldh.maker.vo.TreeNode;

/**
 * Created by ldh on 2018/6/7.
 */
public class JavafxSettingPane extends TabPane {

    protected TreeItem<TreeNode> treeItem;
    protected String dbName;
//    private PackagePane packagePane;
    private AliasTablePane aliasTablePane;
    private TableNoPane noTablePane;

    public JavafxSettingPane(TreeItem<TreeNode> treeItem, String dbName) {
        this.treeItem = treeItem;
        this.dbName = dbName;

        this.setSide(Side.TOP);

//        buildPackagePaneTab();
        buildNoTablePaneTab();
        buildAliasPaneTab();
    }

    public boolean isSetting() {
        return true;
    }

    private void buildNoTablePaneTab() {
        noTablePane = new TableNoPane(treeItem, dbName);
        Tab tab = createTab("不生成的Table", noTablePane);
        tab.selectedProperty().addListener((b,o,n)->{
            if (tab.isSelected()) {
                noTablePane.show();
            }
        });
    }

    private void buildAliasPaneTab() {
        aliasTablePane = new AliasTablePane(treeItem, dbName);
        Tab tab = createTab("别名设置", aliasTablePane);
        tab.selectedProperty().addListener((b,o,n)->{
            if (tab.isSelected()) {
                aliasTablePane.show();
            }
        });
    }

    protected Tab createTab(String tabName, Node node) {
        Tab tab = new Tab(tabName);
        tab.setContent(node);
        tab.setClosable(false);
        this.getTabs().add(tab);
        return tab;
    }
}
