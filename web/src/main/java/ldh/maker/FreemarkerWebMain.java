package ldh.maker;

import ldh.maker.component.BootstrapContentUiFactory;
import ldh.maker.component.FreemarkerContentUiFactory;
import ldh.maker.util.UiUtil;

public class FreemarkerWebMain extends ServerMain {

    @Override
    protected void preHandle() {
        UiUtil.setContentUiFactory(new FreemarkerContentUiFactory());
        UiUtil.setType("freemarker");
    }

    @Override
    protected String getTitle() {
        return "智能代码生成器之生成spring boot + freemarker代码 ";
    }

    public static void main(String[] args) throws Exception {
        startDb(null);
        launch(args);
    }
}

