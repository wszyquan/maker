package ldh.maker.code;

import javafx.scene.control.TreeItem;
import ldh.database.Table;
import ldh.maker.database.TableInfo;
import ldh.maker.freemaker.*;
import ldh.maker.util.DbInfoFactory;
import ldh.maker.util.FileUtil;
import ldh.maker.util.FreeMakerUtil;
import ldh.maker.vo.DBConnectionData;
import ldh.maker.vo.SettingData;
import ldh.maker.vo.SettingJson;
import ldh.maker.vo.TreeNode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ldh on 2017/4/6.
 */
public class FreemarkerCreateCode extends WebCreateCode {

    public FreemarkerCreateCode(SettingData data, TreeItem<TreeNode> treeItem, String dbName, Table table) {
        super(data, treeItem, dbName, table);
        add("/freemarker/head.ftl", "head.ftl", "ftl", "common");
        add("/freemarker/left.ftl", "left.ftl", "ftl", "common");
        add("/freemarker/jspMain.ftl", "main.ftl", "ftl");
        add("/freemarker/login.ftl", "login.ftl", "ftl");

//        add("/freemarker/jspList.ftl", "jspList.ftl", "templates");
        addData("controllerFtl", "/freemarker/controller.ftl");
//        addData("freemarkerFtl", "/freemarker/freemarkerConfig.ftl");
//        addData("jspListFtl", "jspList.ftl");
//        jsFtls.add("/bootstrap/jsList.ftl");

        if (data.getSettingJson().isShiro()) {
            add("/freemarker/login.ftl", "login.ftl", "ftl");
        }
    }

    @Override
    protected void createOther(){
        super.createOther();

        String projectRootPackage = getProjectRootPackage(data.getPojoPackageProperty());
        String configPath = createPath(projectRootPackage + ".configuration");
        new FreemarkerConfigMaker()
                .basePackage(projectRootPackage)
                .outPath(configPath)
                .author(data.getAuthorProperty())
                .ftl("/freemarker/freemarkerConfig.ftl")
                .make();

        String db = treeItem.getValue().getData().toString();
        TableInfo tableInfo = DbInfoFactory.getInstance().get(treeItem.getValue().getParent().getId() + "_" + db);
        String t = FreeMakerUtil.firstLower(table.getJavaName());
        new JspMaker()
                .tableInfo(tableInfo)
                .table(table)
                .outPath(createFtlPath(FreeMakerUtil.javaName(table.getName())))
                .ftl("/freemarker/jspList.ftl")
                .fileName(t + "List.ftl")
                .make();

        new JspMaker()
                .tableInfo(tableInfo)
                .table(table)
                .outPath(createFtlPath(FreeMakerUtil.javaName(table.getName())))
                .ftl("/freemarker/jspView.ftl")
                .fileName(t + "View.ftl")
                .make();

        new JspMaker()
                .tableInfo(tableInfo)
                .table(table)
                .outPath(createFtlPath(FreeMakerUtil.javaName(table.getName())))
                .ftl("/freemarker/jspEdit.ftl")
                .fileName(t + "Edit.ftl")
                .make();


    }

    @Override
    protected void createOnce(){
        super.createOnce();

        String controllerPath = createPath(data.getControllerPackageProperty());
        new MainControllerMaker()
                .controllerPackage(data.getControllerPackageProperty())
                .author(data.getAuthorProperty())
                .ftl("freemarker/mainController.ftl")
                .fileName("MainController.java")
                .outPath(controllerPath)
                .make();

        new SimpleJavaMaker()
                .auhtor(data.getAuthorProperty())
                .ftl("/freemarker/LoginController.ftl")
                .fileName("LoginController.java")
                .data("package", data.getControllerPackageProperty())
                .outPath(createPath(data.getControllerPackageProperty()))
                .make();

        List<String> dirs = new ArrayList<>(Arrays.asList("code", root, this.getProjectName(), "src", "main", "resources"));
        try {
            SettingJson settingJson = data.getSettingJson();
            if (settingJson.isShiro()) {
                makerShiro();
            }
            copyResources(dirs);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void makerShiro() {
        String projectRootPackage = getProjectRootPackage(data.getPojoPackageProperty());
        String resourcePath = createPath(projectRootPackage + ".configuration");
        new SimpleJavaMaker()
                .projectRootPackage(projectRootPackage)
                .auhtor(data.getAuthorProperty())
                .ftl("/freemarker/AuthRealm.ftl")
                .fileName("AuthRealm.java")
                .outPath(resourcePath)
                .make();

        new SimpleJavaMaker()
                .auhtor(data.getAuthorProperty())
                .ftl("/freemarker/LoginController.ftl")
                .fileName("LoginController.java")
                .data("package", data.getControllerPackageProperty())
                .outPath(createPath(data.getControllerPackageProperty()))
                .make();

        new SimpleJavaMaker()
                .projectRootPackage(projectRootPackage)
                .auhtor(data.getAuthorProperty())
                .ftl("/freemarker/ShiroConfiguration.ftl")
                .fileName("ShiroConfiguration.java")
                .outPath(resourcePath)
                .make();
    }

    private void copyResources(List<String> dirs) throws IOException {
        copyResources("common/js", dirs, "resource", "common", "js");
        copyResources("macro", dirs, "templates", "macro");
        copyResources("common/bootstrap", dirs, "resource", "common");
//            copyResources("common/bootstrap", dirs, "resource", "common");
        copyResources("frame", dirs, "resource", "frame");
    }

    @Override
    protected void buildPomXmlMaker(String path) {
        String projectRootPackage = getProjectRootPackage(data.getPojoPackageProperty());
        String resourcePath = createPomPath();
        new PomXmlMaker()
                .projectRootPackage(projectRootPackage)
                .projectName(this.getProjectName())
                .ftl("/freemarker/pom.ftl")
                .outPath(resourcePath)
                .make();
    }

    protected void buildApplicationPropertiesMaker() {
        String projectRootPackage = getProjectRootPackage(data.getPojoPackageProperty());
        String resourcePath = createResourcePath();
        TreeNode treeNode = treeItem.getValue().getParent();
        DBConnectionData data = (DBConnectionData) treeNode.getData();
        new ApplicationPropertiesMaker()
                .projectRootPackage(projectRootPackage)
                .dBConnectionData(data)
                .ftl("freemarker/applicationProperties.ftl")
                .dbName(dbName)
                .outPath(resourcePath)
                .make();

        resourcePath = createResourcePath("resources-dev");
        new ApplicationPropertiesMaker()
                .projectRootPackage(projectRootPackage)
                .dBConnectionData(data)
                .ftl("freemarker/applicationProperties.ftl")
                .dbName(dbName)
                .outPath(resourcePath)
                .make();

        resourcePath = createResourcePath("resources-prod");
        new ApplicationPropertiesMaker()
                .projectRootPackage(projectRootPackage)
                .dBConnectionData(data)
                .ftl("freemarker/applicationProperties.ftl")
                .dbName(dbName)
                .outPath(resourcePath)
                .make();

    }

    protected void buildApplicationYmlMaker() {
        String projectRootPackage = getProjectRootPackage(data.getPojoPackageProperty());
        String resourcePath = createResourcePath();
        TreeNode treeNode = treeItem.getValue().getParent();
        DBConnectionData data = (DBConnectionData) treeNode.getData();
        new ApplicationPropertiesMaker()
                .projectRootPackage(projectRootPackage)
                .projectName(getProjectName())
                .dBConnectionData(data)
                .ftl("freemarker/application.ftl")
                .dbName(dbName)
                .fileName("application.yml")
                .outPath(resourcePath)
                .make();

        resourcePath = createResourcePath("resources-dev");
        new ApplicationPropertiesMaker()
                .projectRootPackage(projectRootPackage)
                .projectName(getProjectName())
                .dBConnectionData(data)
                .ftl("freemarker/application.ftl")
                .fileName("application.yml")
                .dbName(dbName)
                .outPath(resourcePath)
                .make();

        resourcePath = createResourcePath("resources-prod");
        new ApplicationPropertiesMaker()
                .projectRootPackage(projectRootPackage)
                .projectName(getProjectName())
                .dBConnectionData(data)
                .ftl("freemarker/application.ftl")
                .fileName("application.yml")
                .dbName(dbName)
                .outPath(resourcePath)
                .make();

        resourcePath = createTestResourcePath("resources");
        new ApplicationPropertiesMaker()
                .projectRootPackage(projectRootPackage)
                .projectName(this.getProjectName())
                .dBConnectionData(data)
                .ftl("applicationTest.ftl")
                .fileName("application.yml")
                .dbName(dbName)
                .outPath(resourcePath)
                .make();

    }

    protected String createJspPath(String... dirs) {
        String path = FileUtil.getSourceRoot();
        List<String> dirst = new ArrayList<>(Arrays.asList("code", root, getProjectName(), "src", "main", "resources"));
        for (String d : dirs) {
            dirst.add(d.toString());
        }
        path = makePath(path, dirst);
        return path;
    }

    protected String createJsPath() {
        String path = FileUtil.getSourceRoot();
        List<String> dirs = new ArrayList<>(Arrays.asList("code", root, getProjectName(), "src", "main", "resources"));
        path = makePath(path, dirs);
        return path;
    }

    protected String createJspPath() {
        String path = FileUtil.getSourceRoot();
        List<String> dirs = new ArrayList<>(Arrays.asList("code", root, getProjectName(), "src", "main", "resources", "templates", "ftl"));
        path = makePath(path, dirs);
        return path;
    }

    public String getProjectName() {
        return data.getProjectNameProperty();
    }
}
