package db;

import javafx.beans.property.StringProperty;
import ldh.database.*;
import ldh.maker.database.TableInfo;
import ldh.maker.util.ConnectionFactory;
import ldh.maker.util.FreeMakerUtil;
import ldh.maker.vo.DBConnectionData;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.security.KeyStore;
import java.sql.Connection;
import java.util.Map;

/**
 * Created by ldh on 2017/3/29.
 */
public class DbInfoTest {

    public static void main(String[] args) throws Exception {
        DBConnectionData data = new DBConnectionData();
        data.setUserNameProperty("root");
        data.setPasswordProperty("123456");
        data.setIpProperty("localhost");
        data.setPortProperty(3306);
        Connection connection = ConnectionFactory.getConnection(data);
        TableInfo tableInfo = new TableInfo(connection, "school", null, true);
        for (Map.Entry<String, Table> entry : tableInfo.getTables().entrySet()) {
            System.out.println(entry.getKey());
            Table table = entry.getValue();
            if (table.isMiddle()) {
                System.out.println("middle:" + table.getName());
                continue;
            }
            for (Column ui : table.getColumnList()) {
                System.out.println("\t, text:" + ui.getText());
            }
            for (UniqueIndex ui : table.getIndexies()) {
                System.out.println("\t, ui:" + FreeMakerUtil.uniqueName(ui));
            }

            for (ForeignKey fk : table.getForeignKeys()) {
                System.out.println("\t, fk:" + fk.getColumnName() + "," + fk.getForeignTable().getName());
            }

            for (ManyToMany mtm : table.getManyToManys()) {
                System.out.println("\t, mtm:" + mtm.getPrimaryTable().getName() + "(" + mtm.getPrimaryForeignKey().getColumn().getName() + ")"
                    + "-" + mtm.getMiddleTable().getName() + "-" + mtm.getSecondTable().getName() + "("
                    + mtm.getSecondForeignKey().getColumn().getName() + ")");
            }

            for (ForeignKey fk : table.getMany()) {
                System.out.println("\t, many:" + fk.getTable().getName() + "(" + fk.getColumn().getName() + ")" + fk.isOneToOne() + "." + fk.getColumn().isOneToOne());
            }
        }

        System.out.println(tableInfo.getTables().size());
        Map<String, Boolean> map = FreeMakerUtil.getAllFunction(tableInfo.getTable("teacher"));
        for (Map.Entry<String, Boolean> entry : map.entrySet()) {
            System.out.println(entry.getKey());
        }
        System.out.println(map.size());

        connection.close();
        String p = "org.test.pojo";
        String tts[] = p.split("\\.");
        System.out.println("sfadsa");

        String pp = p.replace(".", "/");
        System.out.println(pp);

        String str = "demo(12)";
        int idx1 = str.indexOf("(");
        int idx2 = str.indexOf(")");
        String name = str.substring(0, idx1);
        String value = str.substring(idx1+1, idx2);
        System.out.println(name + ":" + value);

    }
}
