<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE configuration PUBLIC "-//mybatis.org//DTD Config 3.0//EN" "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>

    <settings>
        <setting name="logImpl" value="STDOUT_LOGGING" />
    </settings>

    <typeAliases>
        <#list tables as table>
        <#if table.create>
        <typeAlias alias="${util.firstUpper(table.javaName)}" type="${pojoPackage}.${util.firstUpper(table.javaName)}" />
        </#if>
        </#list>
        <#list tables as table>
        <#if !table.middle && table.create>
        <typeAlias alias="${util.firstUpper(table.javaName)}Where" type="${pojoPackage}.where.${util.firstUpper(table.javaName)}Where" />
        </#if>
        </#list>
    </typeAliases>

    <typeHandlers>
        <#list enumList as el>
        <typeHandler handler="ldh.common.mybatis.ValuedEnumTypeHandler"  javaType="${el}"/>
        </#list>
    </typeHandlers>
</configuration>