package ldh.maker;

import com.jfoenix.controls.JFXDecorator;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import ldh.fx.transition.FadeOutUpBigTransition;
import ldh.maker.util.FileUtil;
import ldh.maker.util.UiUtil;

import java.io.File;
import java.io.IOException;
import java.sql.*;

/**
 * Created by ldh on 2017/2/18.
 */
public abstract class ServerMain extends Application {

    private Node root = null;

    @Override
    public void start(Stage primaryStage) throws Exception {
        UiUtil.STAGE = primaryStage;
        if (root == null) initNode(primaryStage);
        JFXDecorator jfxDecorator = (JFXDecorator) root;

        jfxDecorator.setOnCloseButtonAction(()->{
            FadeOutUpBigTransition fadeOutUpBigTransition = new FadeOutUpBigTransition(jfxDecorator);
            fadeOutUpBigTransition.setOnFinished(e->primaryStage.close());
            fadeOutUpBigTransition.playFromStart();
        });
        Scene scene = new Scene(jfxDecorator, 1200, 700);
        scene.setFill(null);


        scene.getStylesheets().add("/styles/bootstrapfx.css");
        scene.getStylesheets().add("/styles/java-keywords.css");
        scene.getStylesheets().add("/styles/xml-highlighting.css");
//        scene.getStylesheets().add(ServerMain.class.getResource("/styles/jfoenix-components.css").toExternalForm());
        scene.getStylesheets().add("/styles/Main.css");
//        scene.getStylesheets().add(ServerMain.class.getResource("/styles/jfoenix-design.css").toExternalForm());

        primaryStage.setTitle(getTitle());
        primaryStage.setScene(scene);
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        jfxDecorator.setOpacity(0);

//        primaryStage.setScene(scene);
        primaryStage.show();

        primaryStage.setOnCloseRequest(e->{
            try {
                UiUtil.H2CONN.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        });

        preHandle();
    }

    public void initNode(Stage stage) throws IOException {
        Parent main = FXMLLoader.load(getClass().getResource("/fxml/Main.fxml"));

        JFXDecorator jfxDecorator = new JFXDecorator(stage, main);
        root = jfxDecorator;
        jfxDecorator.setOnCloseButtonAction(()->{
            try {
                UiUtil.H2CONN.close();
                System.exit(0);
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        });
    }

    public Node getRoot() {
        return root;
    }

    protected abstract void preHandle();

    protected String getTitle() {
        return "微服务端--代码自动生成工具";
    }

    public static void main(String[] args) throws Exception {
        startDb(null);
        launch(args);
    }

    public static void startDb(String dbName) throws Exception {
//        Server server = Server.createTcpServer(args).start();
        dbName = dbName == null ? "db" : dbName;
        Class.forName("org.h2.Driver");
        String file = FileUtil.getSourceRoot() + "/data";
        File f = new File(file);
        while(!f.exists()) {
            f.mkdir();
        }
        file += "/" + dbName;
//        file = "E:\\logs\\maker";
        Connection conn = DriverManager.getConnection("jdbc:h2:" + file, "sa", "");
        initDb(conn);
        UiUtil.H2CONN = conn;
    }

    private static void initDb(Connection conn) throws Exception {
        String sql = "select count(*) from tree_node";
        Statement statement = conn.createStatement();
        boolean hasExist = true;
        try {
//            deleteTables(statement, "tree_node", "db_info");
            ResultSet resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {

            }
        } catch (Exception e) {
            hasExist = false;
        }
        if (hasExist) {
            statement.close();
            return;
        }
        sql = FileUtil.loadJarFile("/data.sql");
        System.out.println("sql:::::::" + sql);
        statement.execute(sql);
        statement.close();
    }

    public static void deleteTables(Statement statement, String... tables) throws Exception {
        for (String table : tables) {
            statement.execute("DROP table " + table);
        }
    }
}

