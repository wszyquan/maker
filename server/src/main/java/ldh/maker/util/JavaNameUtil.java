package ldh.maker.util;

/**
 * Created by ldh on 2017/3/26.
 */
public class JavaNameUtil {

    public static String className(String str) {
        String t = replace(str, "_");
        t = replace(t, "-");
        return t.substring(0, 1).toUpperCase() + t.substring(1);
    }

    public static String propertyName(String str) {
        String t = replace(str, "_");
        t = replace(t, "-");
        return t;
    }

    private static String replace(String str, String split) {
        String[] strs = str.split("_");
        StringBuilder sb = new StringBuilder();
        for(int i=0; i<strs.length; i++) {
            if (i == 0) {
                sb.append(strs[i]);
                continue;
            }
            String t = strs[i].substring(0, 1).toUpperCase() + strs[i].substring(1);
            sb.append(t);
        }
        return sb.toString();
    }
}
